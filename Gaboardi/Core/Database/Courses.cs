﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Core.Database
{
    [Table(Global.TablePrefix + "Courses")]
    public class Courses : DefaultDatabase<Courses>
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string link { get; set; }
    }
}