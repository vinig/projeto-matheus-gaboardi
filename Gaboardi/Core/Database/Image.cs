﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Core.Database
{
    [Table(Global.TablePrefix + "Image")]
    public class Image : DefaultDatabase<Image>
    {
        public int id { get; set; }
        public string image { get; set; }
        public string area { get; set; }

        public List<Image> ListBrasil()
        {
            return ExecuteQuery<Image>(@"SELECT * FROM GA_Image WHERE id <= 8");
        }
        public List<Image> ListExterior()
        {
            return ExecuteQuery<Image>(@"SELECT * FROM GA_Image WHERE id > 8 and id <= 16");
        }
        public List<Image> ListGrupo()
        {
            return ExecuteQuery<Image>(@"SELECT * FROM GA_Image WHERE id > 16");
        }
    }
}