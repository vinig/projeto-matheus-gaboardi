﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Core.Database
{
    [Table(Global.TablePrefix + "Materials")]
    public class Materials : DefaultDatabase<Materials>
    {
        public int id { get; set; }
        public string title { get; set; }
        public int area { get; set; }
        public string description { get; set; }
        public string pdf { get; set; }
        public string image { get; set; }
    }
}