﻿using System;
using Dapper;

namespace Core.Database.System
{
    [Table(Global.TablePrefix + "Logs")]
    public class Logs : DefaultDatabase<Logs>
    {
        [Key]
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public string Query { get; set; }
    }
}