﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Core.Database
{
    [Table(Global.TablePrefix + "Text")]
    public class Text:DefaultDatabase<Text>
    {
        public int id { get; set; }
        public string image { get; set; }
        public string area { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string pdf { get; set; }
    }
}