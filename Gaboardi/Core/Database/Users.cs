﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Core.Database
{
    [Table(Global.TablePrefix + "Users")]
    public class Users:DefaultDatabase<Users>
    {
        public int id { get; set; }
        public string user { get; set; }
        public string pass { get; set; }
        public string name { get; set; }
    }
}