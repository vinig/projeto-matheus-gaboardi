﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Core.Helpers
{
    public class Pagination<TEntity> where TEntity : class
    {
        public List<TEntity> lstObj { get; set; }
        public int totalPages { get; set; }

        public Pagination<TEntity> PaginateEverything(int PageSize, List<TEntity> obj, int page) 
        {
            page = page - 1;
            var count = obj.Count();
            var data = obj.Skip(page * PageSize).Take(PageSize).ToList();

            var totalPages = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            return new Pagination<TEntity>() {lstObj = data, totalPages = totalPages };
        }
    }
}
