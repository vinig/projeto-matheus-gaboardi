﻿using Core.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace DevBaseNET.Controllers
{
    public class GAController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        //public ActionResult MentoriaIndividualBrasil()
        //{
        //    return View();
        //}
        //public ActionResult MentoriaIndividualExterior()
        //{
        //    return View();
        //}
        //public ActionResult MentoriaGrupo()
        //{
        //    return View();
        //}
        public ActionResult MateriaisGratuitos()
        {
            return View();
        }
        public ActionResult Videos()
        {
            return View();
        }
        public ActionResult FaleConosco()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendEmail(string name, string email, string fone, string description)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("contato@rdevit.com", "Contato Site Matheus Gaboardi");
                String corpo = (@"<style type='text/css'> small { font-family: Verdana, sans-serif; } </style>" +
                @"<div style = 'text-align: left;'>" +
                  @"<h3> Nome: <small> " + name + " </small></h3>" +
                  @"<h3> E-mail: <small> " + email + " </small></h3>" +
                  @"<h3> Telefone: <small> " + fone + " </small></h3>" +
                  @"<h3> Mensagem: <small> " + description + " </small></h3>" +
                @"</div>");
                mailMessage.To.Add(new Text().Get(41).link);
                mailMessage.Subject = "Contato de " + name;
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = corpo.ToString();
                mailMessage.Priority = MailPriority.High;
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
                smtpClient.EnableSsl = true;
                smtpClient.Port = 587;
                smtpClient.Credentials = new NetworkCredential("disparadorviniciusgoncalves@gmail.com", "larissa1808");
                smtpClient.Send(mailMessage);
                TempData["Email"] = "E-mail Enviado com sucesso!";
            }
            catch(Exception ex)
            {
                TempData["Email"] = "Erro!";
            }
            return RedirectToAction("FaleConosco");
        }
        public ActionResult Cursos()
        {
            return View();
        }
        public ActionResult Mentorias()
        {
            return View();
        }
    }
}