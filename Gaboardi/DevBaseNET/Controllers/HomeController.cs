﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Database;
using Core.Database.System;
using System;
using System.Collections.Generic;
using System.IO;
using Core;
using Newtonsoft;
using RestSharp;

namespace DevBaseNET.Controllers
{
    public class HomeController : Controller
    {
        public static Users currentUser = Helpers.UserLogin.GetUser();

        public string currentRoute() => HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            try
            {
                HttpPostedFileBase file = Request.Files[0] as HttpPostedFileBase;
                //new Logs().ExecuteQuery("INSERT INTO GA_Logs(Message) Values (@file)", new { file });
                if (file == null)
                    return Json(new { name = "" }, JsonRequestBehavior.AllowGet);
                string fileName = DateTime.Now.ToLongTimeString().Replace("/", "").Replace(":", "").Replace(" ", "") + file.FileName.Substring(file.FileName.LastIndexOf("."));
                string path = Path.Combine(Server.MapPath("~/arquivos/"), Path.GetFileName(fileName));
                file.SaveAs(path);
                //new Logs().ExecuteQuery("INSERT INTO GA_Logs(Message,Query) Values (@fileName,'FileName')", new { fileName });
                //new Logs().ExecuteQuery("INSERT INTO GA_Logs(Message,Query) Values (@path,'Path')", new { path });
                return Json(new { name = fileName }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                new Logs().ExecuteQuery("INSERT INTO GA_Logs(Message,Query) Values (@Message,'Catch')", new { ex.Message });
                return Json(new { name = "" }, JsonRequestBehavior.AllowGet);
            }
        }   

        public HomeController()
        {  
            try
            {
                ViewData["User"] = Helpers.UserLogin.GetUser();
            }
            catch
            {
                ViewData["User"] = null;
            }
        }

      
        public void validateSession()
        {
            if (Helpers.UserLogin.GetUser() == null)
                Response.Redirect("/Home/Login");
        }

        public ActionResult Login() => View();

        public ActionResult Index()
        {
            validateSession();
            return View();
        }

        [HttpPost]
        public ActionResult Login(string user, string pass)
        {
            //var bdUser = new Users().GetByCondition("where [user] = @user AND pass = @pass",
            //    new { user, pass });
            var bdUser = new Users().All().Where(x => x.user == user && x.pass == pass);
            if (bdUser.Any())
            {
                Helpers.UserLogin.SetUser(bdUser.First());
                return RedirectToAction("Index");
            }
            return RedirectToAction("Login");
        }

        //[HttpPost]
        public ActionResult Text(Text obj, int? objId = null, bool isInsert = false, bool isDelete = false)
        {
            validateSession();
            ViewBag.Title = "Textos";
            ViewData["route"] = "Home/" + currentRoute();
            if (isInsert)
                obj.Insert();
            else if (objId != null)
                ViewData["obj"] = new Text().Get((int)objId);
            else if (isDelete)
                obj.Delete();
            else if (obj.id == 0 && isInsert == false)
                ViewData["objs"] = new Text().All();
            else if (obj.id > 0)
                obj.Update();
            if (obj.id > 0 || isInsert) return RedirectToAction(currentRoute());
            else return View();
        }

        //[HttpPost]
        public ActionResult Image(Image obj, int? objId = null, bool isInsert = false, bool isDelete = false)
        {
            validateSession();
            ViewBag.Title = "Mentorias Imagens";
            ViewData["route"] = "Home/" + currentRoute();
            if (isInsert)
                obj.Insert();
            else if (objId != null)
                ViewData["obj"] = new Image().Get((int)objId);
            else if (isDelete)
                obj.Delete();
            else if (obj.id == 0 && isInsert == false)
                ViewData["objs"] = new Image().All();
            else if (obj.id > 0)
                obj.Update();
            if (obj.id > 0 || isInsert) return RedirectToAction(currentRoute());
            else return View();
        }

        //[HttpGet]
        public ActionResult Videos(Videos obj, int? objId = null, bool isInsert = false, bool isDelete = false)
        {
            validateSession();
            ViewBag.Title = "Videos";
            ViewData["route"] = "Home/" + currentRoute();
            if (isInsert)
                obj.Insert();
            else if (objId != null)
                ViewData["obj"] = new Videos().Get((int)objId);
            else if (isDelete)
                obj.Delete();
            else if (obj.id == 0 && isInsert == false)
                ViewData["objs"] = new Videos().All();
            else if (obj.id > 0)
                obj.Update();
            if (obj.id > 0 || isInsert) return RedirectToAction(currentRoute());
            else return View();
        }

        //[HttpPost]
        public ActionResult Courses(Courses obj, int? objId = null, bool isInsert = false, bool isDelete = false)
        {
            validateSession();
            ViewBag.Title = "Cursos";
            ViewData["route"] = "Home/" + currentRoute();
            if (isInsert)
                obj.Insert();
            else if (objId != null)
                ViewData["obj"] = new Courses().Get((int)objId);
            else if (isDelete)
                obj.Delete();
            else if (obj.id == 0 && isInsert == false)
                ViewData["objs"] = new Courses().All();
            else if (obj.id > 0)
                obj.Update();
            if (obj.id > 0 || isInsert) return RedirectToAction(currentRoute());
            else return View();
        }

        //[HttpPost]
        public ActionResult Materials(Materials obj, int? objId = null, bool isInsert = false, bool isDelete = false)
        {
            validateSession();
            ViewBag.Title = "Materiais Gratuitos";
            ViewData["route"] = "Home/" + currentRoute();
            if (isInsert)
                obj.Insert();
            else if (objId != null)
                ViewData["obj"] = new Materials().Get((int)objId);
            else if (isDelete)
                obj.Delete();
            else if (obj.id == 0 && isInsert == false)
                ViewData["objs"] = new Materials().All();
            else if (obj.id > 0)
                obj.Update();
            if (obj.id > 0 || isInsert) return RedirectToAction(currentRoute());
            else return View();
        }
    }
}