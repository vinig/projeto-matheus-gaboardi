﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace DevBaseNET.Helpers
{
    public class Extractor
    {
        public string GetContent(string url)
        {
            using (var wc = new WebClient())
            {
                wc.Headers.Add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                return wc.DownloadString(url);
            }
        }

        public string Clear(string initial)
        {
            return Regex.Replace(Regex.Replace(initial, "<.*?>", String.Empty)
                                        .Replace("\n", " "), @"\s\s+", " ")
                                        .Replace("<div", "")
                                        .Replace("\"\">", "")
                                        .Replace("\"> ", "")
                                        .Replace("\">", "")
                                        .Trim();
        }

        public string ExtractNumber(string input)
        {
            return new String(input.Where(Char.IsDigit).ToArray()).Trim();
        }

        public string ExtractBetween(string text, string start, string end)
        {
            int iStart = text.IndexOf(start);
            iStart = (iStart == -1) ? 0 : iStart + start.Length;
            int iEnd = text.LastIndexOf(end);
            if (iEnd == -1)
                iEnd = text.Length;
            int len = iEnd - iStart;
            return text.Substring(iStart, len);
        }
    }
}