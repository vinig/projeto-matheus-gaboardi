﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using DevBaseNET.Interfaces;

namespace DevBaseNET.Helpers
{
    public abstract class FileReader : IFileReader
    {
        private static DataSet GetDataSet<T>
            (string dataSetName, string tableName, IReadOnlyList<T> objectToDataset, Dictionary<string, Type> columns)
        {
            try
            {
                var ds = new DataSet
                {
                    DataSetName = dataSetName
                };

                var dt = new DataTable
                {
                    TableName = tableName
                };

                if(objectToDataset[0] is IEnumerable)
                {
                    foreach(var item in columns)
                    {
                        dt.Columns.Add(new DataColumn(item.Key, item.Value));
                    }

                    for(var r = 1; r < objectToDataset.Count; r++)
                    {
                        var newRow = dt.NewRow();
                        var row = objectToDataset[r] as List<object>;

                        if(row != null)
                        {
                            for(var c = 0; c < dt.Columns.Count; c++)
                            {
                                if(row[c] != null)
                                {
                                    newRow[c] = row[c];
                                }
                            }
                        }

                        dt.Rows.Add(newRow);
                    }
                }
                else
                {
                    foreach(var property in objectToDataset[0].GetType().GetProperties())
                    {
                        dt.Columns.Add
                            (new DataColumn
                                 (property.Name,
                                  property.PropertyType.UnderlyingSystemType.FullName.Contains("Nullable")
                                      ? typeof(string)
                                      : property.PropertyType));
                    }

                    foreach(var currObject in objectToDataset)
                    {
                        var newRow = dt.NewRow();

                        foreach(var property in currObject.GetType().GetProperties())
                        {
                            newRow[property.Name] = currObject.GetType().GetProperty(property.Name).GetValue(currObject, null);
                        }

                        dt.Rows.Add(newRow);
                    }
                }

                ds.Tables.Add(dt);
                return ds;
            }
            catch(ApplicationException appEx)
            {
                throw new ApplicationException(appEx.Message);
            }
            catch(Exception ex)
            {
                throw new Exception("Erro ao transformar objecto em dataset.", ex);
            }
        }


        private HttpPostedFileBase File { get; }
        protected string FileName { get; }
        protected string FilePath { get; }
        protected List<string> Extensions = new List<string>();

        private string UploadDisk()
        {
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "S3FilesUpload/"))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "S3FilesUpload/");
            }

            var filePath = AppDomain.CurrentDomain.BaseDirectory + "S3FilesUpload/" + Path.GetFileName(FileName);
            File.SaveAs(filePath);
            return filePath;
        }

        protected FileReader(HttpPostedFileBase file, string action = "", int? schoolId = null)
        {
            File = file;
            FileName = Path.GetFileName(file.FileName);
            FilePath = UploadDisk();
        }

        protected FileReader() {}

        public bool ValidExtension()
        {
            return Extensions.Contains(Path.GetExtension(FileName));
        }

        public abstract DataTable ConvertToDatable();

        public OleDbConnection GetConnection()
        {
                string connectionString;
                var extension = Path.GetExtension(FileName);

                switch(extension)
                {
                    case ".xls": //Excel 97-03
                        connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath
                                           + @";Extended Properties=""Excel 8.0;HDR=YES;""";
                        break;
                    case ".xlsx": //Excel 07 ou superior
                        connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath
                                           + @";Extended Properties=""Excel 8.0;HDR=YES;""";
                        break;
                    default:
                        connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + FilePath
                                           + @";Extended Properties=""Excel 8.0;HDR=YES;""";
                        break;
                }

                return new OleDbConnection(connectionString);
        }

        public void ExportToExcel<T>
            (string dataSetName, string tableName, List<T> objectToDataset, Dictionary<string, Type> columns = null)
        {
            try
            {
                if(objectToDataset.Any() == false)
                {
                    throw new ApplicationException("Não existem dados para exportar.");
                }

                if(columns == null && objectToDataset[0] is IEnumerable)
                {
                    columns = (objectToDataset[0] as IEnumerable).Cast<object>()
                        .ToDictionary
                        (property => property.ToString(),
                         property =>
                             property.GetType().UnderlyingSystemType.FullName.Contains("Nullable")
                                 ? typeof(string)
                                 : property.GetType());
                }

                using(var ds = GetDataSet(dataSetName, tableName, objectToDataset, columns))
                {
                    var folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "S3FilesUpload");
                    Directory.CreateDirectory(folder);

                    var path = Path.Combine(folder, ds.DataSetName + ".xlsx");

                    if(System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    // https://github.com/SLaks/ExcelExporter
                    //new ExcelExport().AddSheet(ds.Tables[0]).ExportTo(path);
                    try
                    {
                        HttpContext.Current.Response.ContentType =
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        HttpContext.Current.Response.AppendHeader
                            ("Content-Disposition", "attachment; filename=" + Path.GetFileName(path));
                        HttpContext.Current.Response.TransmitFile(path);
                        HttpContext.Current.Response.End();
                    }
                    finally
                    {
                        System.IO.File.Delete(path);
                    }
                }
            }
            catch(HttpException ex)
            {
                throw new ApplicationException("Conexão interrompida. Erro ao exportar dados para excel.", ex);
            }
            catch(UnauthorizedAccessException)
            {
                throw new ApplicationException("Não foi possível completar a operação. Escola sem permissão de pasta.");
            }
          
        }

        public void ExportToExcelSave<T>
            (string dataSetName, string tableName, List<T> objectToDataset, string pathFolder, Dictionary<string, Type> columns = null)
        {
            try
            {
                if (objectToDataset.Any() == false)
                {
                    throw new ApplicationException("Não existem dados para exportar.");
                }

                if (columns == null && objectToDataset[0] is IEnumerable)
                {
                    columns = (objectToDataset[0] as IEnumerable).Cast<object>()
                        .ToDictionary
                        (property => property.ToString(),
                         property =>
                             property.GetType().UnderlyingSystemType.FullName.Contains("Nullable")
                                 ? typeof(string)
                                 : property.GetType());
                }

                using (var ds = GetDataSet(dataSetName, tableName, objectToDataset, columns))
                {
                    Directory.CreateDirectory(pathFolder);

                    var path = Path.Combine(pathFolder, ds.DataSetName + ".xlsx");

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    //new ExcelExport().AddSheet(ds.Tables[0]).ExportTo(path);
                }
            }
            catch (HttpException ex)
            {
                throw new ApplicationException("Conexão interrompida. Erro ao exportar dados para excel.", ex);
            }
            catch (UnauthorizedAccessException)
            {
                throw new ApplicationException("Não foi possível completar a operação. Escola sem permissão de pasta.");
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao exportar dados para excel.", ex);
            }
        }

    }
}
