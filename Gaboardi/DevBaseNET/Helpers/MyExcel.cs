﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using RestSharp;

namespace DevBaseNET.Helpers
{
    public class MyExcel : FileReader
    {
        public override DataTable ConvertToDatable()
        {
            if (string.IsNullOrEmpty(FileName))
            {
                return null;
            }

            try
            {
                using (var connection = GetConnection())
                {
                    connection.Open();
                    var dtSheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                    if (dtSheet == null)
                    {
                        throw new ApplicationException("Não foi possível lrt o excel.");
                    }

                    var sheet =
                        dtSheet.Rows.Cast<DataRow>().First(drSheet => drSheet["TABLE_NAME"].ToString().Contains("$"))[
                                                                                                                      "TABLE_NAME"
                                                                                                                     ].ToString();
                    connection.Close();

                    if (string.IsNullOrEmpty(sheet))
                    {
                        throw new ApplicationException("Não foi possível ler o excel.");
                    }

                    var dt = new DataSet();
                    new OleDbDataAdapter("SELECT * FROM [" + sheet + "]", connection).Fill(dt);
                    return dt.Tables[0];
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static void ExportExcel
            (HttpContextBase httpContext, string templatePath, string exportFileName, ExportOptions options)
        {
            ExportExcel
                (httpContext,
                 templatePath,
                 exportFileName,
                 excelPackage =>
                 {
                     var sheet = excelPackage.Workbook.Worksheets.First();

                     for (var i = 0; i < options.Data.Count; i++)
                     {
                         sheet.Cells[options.StartRow, options.StartCol, options.StartRow, options.EndCol].Copy
                             (sheet.Cells[i + options.StartRow, options.StartCol, i + options.StartRow, options.EndCol]);

                         sheet.Row(i + options.StartRow).CustomHeight = true;
                         sheet.Row(i + options.StartRow).Height = sheet.Row(options.StartRow).Height;

                         var rowData = options.Data[i];

                         for (var j = options.StartCol; j <= rowData.Count; j++)
                         {
                             var itemData = rowData[j - 1];
                             var hyperlink = itemData as Hyperlink;
                             if (hyperlink != null)
                             {
                                 sheet.Cells[i + options.StartRow, j].Hyperlink = hyperlink.Link;
                                 itemData = hyperlink.Value;
                             }
                             sheet.Cells[i + options.StartRow, j].Value = itemData;
                         }
                     }

                     ResizeTable
                         (sheet.Tables.First(),
                          sheet.Cells[
                                      options.StartTableRow,
                                      options.StartCol,
                                      options.Data.Count + options.StartTableRow,
                                      options.EndCol].Address);
                 });
        }

        public static void ExportExcel
            (HttpContextBase httpContext, string templatePath, string exportFileName, Action<ExcelPackage> action)
        {
            var path = httpContext.Server.MapPath(templatePath);

            if (!File.Exists(path))
            {
                throw new ApplicationException($"Arquivo de modelo não encontrado ({path}).");
            }

            var excelPackage = new ExcelPackage(new FileInfo(path));

            action(excelPackage);

            excelPackage.SaveAs(httpContext.Response.OutputStream);

            httpContext.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            httpContext.Response.AppendHeader("Content-Disposition", $"attachment; filename={exportFileName}");
            httpContext.Response.End();
        }

        public static void ResizeTable(ExcelTable table, string range)
        {
            var tableElement = table.TableXml.DocumentElement;
            if (tableElement != null)
            {
                tableElement.Attributes["ref"].Value = range;
                var autoFilterElement = tableElement["autoFilter"];
                if (autoFilterElement != null)
                {
                    autoFilterElement.Attributes["ref"].Value = tableElement.Attributes["ref"].Value;
                }
            }
        }

        public class ExportOptions
        {
            public int StartTableRow { get; set; }
            public int StartRow { get; set; }
            public int StartCol { get; set; }
            public int EndCol { get; set; }
            public List<List<object>> Data { get; set; }
        }

        public class Hyperlink
        {
            public ExcelHyperLink Link { get; set; }
            public object Value { get; set; }
        }

        public MyExcel() { }

        public MyExcel(HttpPostedFileBase file, string action = "") : base(file, action)
        {
            Extensions = new List<string>
            {
                ".xls",
                ".xlsx",
                ".xml"
            };
        }

        public DataTable GetDataTable()
        {
            return ConvertToDatable();
        }
    }
}
