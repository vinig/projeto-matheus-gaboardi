﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Core.Database;

namespace DevBaseNET.Helpers
{
    public class UserLogin
    {

        internal static void SetUser(Users User)
        {
            FormsAuthentication.SetAuthCookie(User.id.ToString(), false);
        }

        internal static Users GetUser()
        {
            var users = new Users().All();
            var currentUser = new Users().GetByCondition("where id = @id", new { id = GetUserId() });
            if (currentUser.Any())
                return currentUser.First();

            LogoutUser();
            return null;
        }

        internal static void LogoutUser()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Clear();
        }

        internal static int GetUserId()
        {
            var sid = HttpContext.Current.User.Identity.Name;
            int id;
            int.TryParse(sid, out id);
            return id;
        }
    }
}