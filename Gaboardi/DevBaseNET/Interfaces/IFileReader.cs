﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace DevBaseNET.Interfaces
{
    internal interface IFileReader
    {
        DataTable ConvertToDatable();

        bool ValidExtension();

        OleDbConnection GetConnection();

        void ExportToExcel<T>
            (string dataSetName, string tableName, List<T> objectToDataset, Dictionary<string, Type> columns = null);
    }
}
