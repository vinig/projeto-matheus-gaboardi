$(document).ready(function(){
    $.get("json/Footer.json", function (data) {
        $(".titleFooter a").text(data[0].title);
        $(".descriptionFooter").text(data[0].description);

        for(var i=0;i< data[0].recents.length; i++){
            var html = '<p> ' + data[0].recents[i].description + ' <br><small>' + data[0].recents[i].date +'</small></p><hr/>';
            $('.list-info-wthree').append(html);
        }

        $(".addressFooter b").text(data[0].address);
        $(".phoneFooter b").text(data[0].phone);
        $(".emailFooter b").text(data[0].email);
        $(".siteFooter b").text(data[0].site);
        
    });

    $.get("json/Socials.json", function (data) {                    
        $(".facebook").attr("href", data[0].facebook);
        $(".twitter").attr("href", data[0].twitter);
        $(".google").attr("href", data[0].google);
        $(".linkedin").attr("href", data[0].linkedin);
        $(".youtube").attr("href", data[0].youtube);
        $(".instagram").attr("href", data[0].instagram);
        $(".pinterest").attr("href", data[0].pinterest);
    });
});
    