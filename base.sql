USE JJBF
create table GA_Logs (
	Id INT PRIMARY KEY IDENTITY(1,1),
	[Message] TEXT,
	[Date] DATETIME,
	Query text
)


create table GA_Users (
	Id INT PRIMARY KEY IDENTITY(1,1),
	[user] TEXT,
	[pass] text,
	name text
)


create table GA_Text (
	Id INT PRIMARY KEY IDENTITY(1,1),
	image TEXT,
	area text,
	title text,
	description text,
	link text
)

SELECT * FROM GA_Text

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Inicial - Sobre', 'Matheus Gaboardi', 'Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra. Suspendisse dapibus erat lectus rutrum bibendum. Praesent scscelerisque.

Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet dictum nibh viverra iaculis. Sed porta nisl molestie hendrerit. Integer tempus pulvinar laoreet.', null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Inicial - Texto central', 'Tem Muito Conte�do Esperando Por Voc�!', 'Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra Suspendisse. Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet.
Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra Suspendisse. Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet.
Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra Suspendisse. Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet.
Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra Suspendisse. Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet.
Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra Suspendisse. Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet .', null)



INSERT INTO GA_Text VALUES (null, 'Facebook', null, null, 'https://www.facebook.com/')
INSERT INTO GA_Text VALUES (null, 'YouTube', null, null, 'https://www.youtube.com/')
INSERT INTO GA_Text VALUES (null, 'Instagram', null, null, 'https://www.instagram.com/')

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Inicial - Planilha', 'Baixe sua planilha gratuito', 'Praesent sagittis risus
Nam molestie turpis ut rutrum', null)

INSERT INTO GA_Text VALUES (null, 'P�gina V�deos - 1� Video', 'Lorem ipsum dolor sit.', 'Lorem ipsum dolor sit amet, consectetadipiscing elit. Fusce fermentum eros at mi varius sagittis. Nam quis magna
in dui fringilla sodales et at ante. Nulla condimentum.', 'https://www.youtube.com/embed/56jy_mPUVVs')

INSERT INTO GA_Text VALUES (null, 'P�gina V�deos - 2� Video', 'Lorem ipsum dolor sit.', 'Lorem ipsum dolor sit amet, consectetadipiscing elit. Fusce fermentum eros at mi varius sagittis. Nam quis magna
in dui fringilla sodales et at ante. Nulla condimentum.', 'https://www.youtube.com/embed/56jy_mPUVVs')

INSERT INTO GA_Text VALUES (null, 'P�gina V�deos - 3� Video', 'Lorem ipsum dolor sit.', 'Lorem ipsum dolor sit amet, consectetadipiscing elit. Fusce fermentum eros at mi varius sagittis. Nam quis magna
in dui fringilla sodales et at ante. Nulla condimentum.', 'https://www.youtube.com/embed/56jy_mPUVVs')

INSERT INTO GA_Text VALUES (null, 'P�gina Mentoria Individual Brasil', 'Mentoria Individual', 'Praesent sagittis risus nisi, a fermentum tellus aliquam vel. Ut faucibus,
mauris quis fermentum placerat, sem turpis volutpat massa', null)

INSERT INTO GA_Text VALUES (null, 'P�gina Mentoria Individual Brasil - Video', 'Lorem ipsum dolor sit.', 'Lorem ipsum dolor sit amet, consectetadipiscing elit. Fusce fermentum eros at mi varius sagittis. Nam quis magna
in dui fringilla sodales et at ante. Nulla condimentum.', 'https://www.youtube.com/embed/56jy_mPUVVs')

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Mentoria Individual Brasil - 1� Download', 'Praesent sagittis risus
Nam molestie turpis ut rutrum', null, null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Mentoria Individual Brasil - 2� Download', 'Praesent sagittis risus
Nam molestie turpis ut rutrum', null, null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Individual Brasil - 1� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
												Mauris arisusvitae massa consequat<br>
												pharetra Suspendisse.', null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Individual Brasil - 2� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
Mauris arisusvitae massa consequat<br>
pharetra Suspendisse.', null)
INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Individual Brasil - 3� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
Mauris arisusvitae massa consequat<br>
pharetra Suspendisse.', null)


create table GA_Image (
	Id INT PRIMARY KEY IDENTITY(1,1),
	image TEXT,
	area text,
)

select * from GA_Image
SELECT * FROM GA_Image WHERE id > 16

INSERT INTO GA_Image VALUES ('https://i.imgur.com/HDEoOkw.png', 'Mentoria Grupo - 1� Imagem')
INSERT INTO GA_Image VALUES ('https://i.imgur.com/HDEoOkw.png', 'Mentoria Grupo - 2� Imagem')
INSERT INTO GA_Image VALUES ('https://i.imgur.com/HDEoOkw.png', 'Mentoria Grupo - 3� Imagem')
INSERT INTO GA_Image VALUES ('https://i.imgur.com/HDEoOkw.png', 'Mentoria Grupo - 4� Imagem')
INSERT INTO GA_Image VALUES ('https://i.imgur.com/HDEoOkw.png', 'Mentoria Grupo - 5� Imagem')
INSERT INTO GA_Image VALUES ('https://i.imgur.com/HDEoOkw.png', 'Mentoria Grupo - 6� Imagem')
INSERT INTO GA_Image VALUES ('https://i.imgur.com/HDEoOkw.png', 'Mentoria Grupo - 7� Imagem')
INSERT INTO GA_Image VALUES ('https://i.imgur.com/HDEoOkw.png', 'Mentoria Grupo - 8� Imagem')


select * from GA_Text

INSERT INTO GA_Text VALUES (null, 'P�gina Mentoria Individual Exterior', 'Mentoria Individual', 'Praesent sagittis risus nisi, a fermentum tellus aliquam vel. Ut faucibus,
mauris quis fermentum placerat, sem turpis volutpat massa', null)

INSERT INTO GA_Text VALUES (null, 'P�gina Mentoria Individual Exterior - Video', 'Lorem ipsum dolor sit.', 'Lorem ipsum dolor sit amet, consectetadipiscing elit. Fusce fermentum eros at mi varius sagittis. Nam quis magna
in dui fringilla sodales et at ante. Nulla condimentum.', 'https://www.youtube.com/embed/56jy_mPUVVs')

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Mentoria Individual Exterior - 1� Download', 'Praesent sagittis risus
Nam molestie turpis ut rutrum', null, null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Mentoria Individual Exterior - 2� Download', 'Praesent sagittis risus
Nam molestie turpis ut rutrum', null, null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Individual Exterior - 1� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
												Mauris arisusvitae massa consequat<br>
												pharetra Suspendisse.', null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Individual Exterior - 2� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
Mauris arisusvitae massa consequat<br>
pharetra Suspendisse.', null)
INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Individual Exterior - 3� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
Mauris arisusvitae massa consequat<br>
pharetra Suspendisse.', null)

select * from GA_Text

INSERT INTO GA_Text VALUES (null, 'P�gina Mentoria Grupo', 'Mentoria Grupo', 'Praesent sagittis risus nisi, a fermentum tellus aliquam vel. Ut faucibus,
mauris quis fermentum placerat, sem turpis volutpat massa', null)

INSERT INTO GA_Text VALUES (null, 'P�gina Mentoria Grupo - Video', 'Lorem ipsum dolor sit.', 'Lorem ipsum dolor sit amet, consectetadipiscing elit. Fusce fermentum eros at mi varius sagittis. Nam quis magna
in dui fringilla sodales et at ante. Nulla condimentum.', 'https://www.youtube.com/embed/56jy_mPUVVs')

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Mentoria Grupo - 1� Download', 'Praesent sagittis risus
Nam molestie turpis ut rutrum', null, null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/A5qBpZv.png', 'P�gina Mentoria Grupo - 2� Download', 'Praesent sagittis risus
Nam molestie turpis ut rutrum', null, null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Grupo - 1� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
												Mauris arisusvitae massa consequat<br>
												pharetra Suspendisse.', null)

INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Grupo - 2� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
Mauris arisusvitae massa consequat<br>
pharetra Suspendisse.', null)
INSERT INTO GA_Text VALUES ('https://i.imgur.com/dLZXJyE.png', 'P�gina Mentoria Grupo - 3� Depoimento', 'Nome da pessoa', 'Suspendisse commodo auctor molestie.<br>
Mauris arisusvitae massa consequat<br>
pharetra Suspendisse.', null)



INSERT INTO GA_Text VALUES (null, 'P�gina Materiais Gratuitos - 1� Material', 'Nullam Porttitor Sagittis Ligula.', 
'Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra. Suspendisse dapibus erat lectus rutrum bibendum. Praesent scscelerisque.

Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet dictum nibh viverra iaculis. Sed porta nisl molestie hendrerit. Integer tempus pulvinar laoreet.

Cras imperdiet nibh nibh, ut cursus ligula interdum in. Proin at ullamcorper felis, quis gravida quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ut maximus ipsum. Aliquam eget luctus magna.', null)

INSERT INTO GA_Text VALUES (null, 'P�gina Materiais Gratuitos - 2� Material', 'Curabitur Mattis Magna At Felis.', 
'Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet dictum nibh viverra iaculis. Sed porta nisl molestie hendrerit. Integer tempus pulvinar laoreet.

Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra. Suspendisse dapibus erat lectus rutrum bibendum. Praesent scscelerisque.

Cras imperdiet nibh nibh, ut cursus ligula interdum in. Proin at ullamcorper felis, quis gravida quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ut maximus ipsum. Aliquam eget luctus magna.', null)


INSERT INTO GA_Text VALUES (null, 'P�gina Materiais Gratuitos - 3� Material', 'Curabitur Mattis Magna At Felis.', 
'Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet dictum nibh viverra iaculis. Sed porta nisl molestie hendrerit. Integer tempus pulvinar laoreet.

Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra. Suspendisse dapibus erat lectus rutrum bibendum. Praesent scscelerisque.

Cras imperdiet nibh nibh, ut cursus ligula interdum in. Proin at ullamcorper felis, quis gravida quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ut maximus ipsum. Aliquam eget luctus magna.', null)


INSERT INTO GA_Text VALUES (null, 'P�gina Materiais Gratuitos - 4� Material', 'Curabitur Mattis Magna At Felis.', 
'Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet dictum nibh viverra iaculis. Sed porta nisl molestie hendrerit. Integer tempus pulvinar laoreet.

Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra. Suspendisse dapibus erat lectus rutrum bibendum. Praesent scscelerisque.

Cras imperdiet nibh nibh, ut cursus ligula interdum in. Proin at ullamcorper felis, quis gravida quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ut maximus ipsum. Aliquam eget luctus magna.', null)

INSERT INTO GA_Text VALUES (null, 'P�gina Cursos - 1� Curso', 'Curabitur Mattis Magna At Felis.', 
'Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet dictum nibh viverra iaculis. Sed porta nisl molestie hendrerit. Integer tempus pulvinar laoreet.

Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra. Suspendisse dapibus erat lectus rutrum bibendum. Praesent scscelerisque.

Cras imperdiet nibh nibh, ut cursus ligula interdum in. Proin at ullamcorper felis, quis gravida quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ut maximus ipsum. Aliquam eget luctus magna.', null)

INSERT INTO GA_Text VALUES (null, 'P�gina Cursos - 2� Curso', 'Curabitur Mattis Magna At Felis.', 
'Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet dictum nibh viverra iaculis. Sed porta nisl molestie hendrerit. Integer tempus pulvinar laoreet.

Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra. Suspendisse dapibus erat lectus rutrum bibendum. Praesent scscelerisque.

Cras imperdiet nibh nibh, ut cursus ligula interdum in. Proin at ullamcorper felis, quis gravida quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ut maximus ipsum. Aliquam eget luctus magna.', null)

INSERT INTO GA_Text VALUES (null, 'P�gina Cursos - 3� Curso', 'Curabitur Mattis Magna At Felis.', 
'Fusce vel enim sapien. Vivamus pellentesque hendrerit erat, sit amet dictum nibh viverra iaculis. Sed porta nisl molestie hendrerit. Integer tempus pulvinar laoreet.

Suspendisse commodo auctor molestie. Mauris arisusvitae massa consequat pharetra. Suspendisse dapibus erat lectus rutrum bibendum. Praesent scscelerisque.

Cras imperdiet nibh nibh, ut cursus ligula interdum in. Proin at ullamcorper felis, quis gravida quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ut maximus ipsum. Aliquam eget luctus magna.', null)
